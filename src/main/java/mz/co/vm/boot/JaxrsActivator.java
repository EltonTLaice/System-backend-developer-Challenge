package mz.co.vm.boot;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.h2.jdbcx.JdbcDataSource;

import mz.co.vm.models.RandomNumber;

@ApplicationPath("/")
public class JaxrsActivator extends Application {

	
	@PostConstruct
	public void init() {

		Properties props = System.getProperties();
		props.setProperty("deployment.resource.validation", "false") ; 
	}
}
