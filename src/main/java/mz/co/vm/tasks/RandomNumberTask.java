package mz.co.vm.tasks;

import mz.co.vm.models.RandomNumber;

public class RandomNumberTask implements Runnable{
	
	private String requestId;
	public RandomNumberTask(String requestId) {
		this.requestId = requestId;
	}
	@Override
	public void run() {
		RandomNumber randomNumber = new RandomNumber(this.requestId);
	}

}
