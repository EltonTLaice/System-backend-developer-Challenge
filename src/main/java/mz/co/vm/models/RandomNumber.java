package mz.co.vm.models;

import java.io.Serializable;
import java.util.Random;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class RandomNumber implements Serializable{
	
	@Id
	private String requestID;
	
	@Column(nullable = false)
	private Long number;
	
	@Column(nullable = false)
	private double process_time;
	
	public RandomNumber() {}
	public RandomNumber(String requestID) {
		this.requestID = requestID;
		
		Random rn = new Random();
		this.number = rn.nextLong();
	}
	public String getRequestID() {
		return requestID;
	}

	public Long getNumber() {
		return number;
	}

	public double getProcess_time() {
		return process_time;
	}
	public void setProcess_time(int process_time) {
		this.process_time = process_time;
	}
}
