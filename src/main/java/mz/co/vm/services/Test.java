package mz.co.vm.services;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Test {

	public static void main(String[] args) {
		Executor executor = Executors.newFixedThreadPool(10);
		executor.execute(() -> System.out.println("Single thread pool test"));
	}

}
