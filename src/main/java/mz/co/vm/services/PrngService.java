package mz.co.vm.services;

import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.json.Json;
import javax.json.JsonObject;

/**
 * Services for PRNG resources
 * 
 * @author Elton Tomas Laice
 * @see mz.co.vm.controllers.PrngController
 */
@ApplicationScoped
public class PrngService {
	/**
	 * Success information if random number was created
	 * @return json
	 */
	public JsonObject success_JsonRandomNumber(String requestId) {
    	JsonObject json = Json.createObjectBuilder()
 			   .add("status", "success")
 			   .add("requestId", requestId)
 			   .add("message", "Request created.")
 			   .build();
    	return json;
	}
	
	/**
	 * The IDs generated must be 128-bit GUIDs
	 * @return requestID
	 */
	public String create_id_request() {
		UUID uuid = UUID.randomUUID();
		String requestID = uuid.toString();
		return requestID;
	}
}
